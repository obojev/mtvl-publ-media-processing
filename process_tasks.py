import json
import psycopg2
import fcntl
import time
import sys
import ConfigParser
import logging
from logging.handlers import TimedRotatingFileHandler
import os
from decimal import *
from shutil import copy, rmtree
from psycopg2.extensions import adapt
import tempfile
import uuid
import hashlib
from common import RepeatedTimer
import ntpath

def md5_for_file(f, block_size=2**20):
    md5 = hashlib.md5()
    while True:
        data = f.read(block_size)
        if not data:
            break
        md5.update(data)
    return str(md5.hexdigest())


def os_run_command(command):
    logging.info(command)

    result = True

    import subprocess
    try:
        result = subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as spe:
        logging.error('=' * 20)
        logging.error(spe.output)
        logging.error('=' * 20)
        result = False

    return result

def update_task_status_error(id, message):
    # updating status, executing
    print '===' + message
    cur.execute("UPDATE tasks SET status='error', updated_utc=now() at time zone 'utc', try_qty=try_qty+1, message={message} \
        WHERE id={id}".format(message=adapt(message), id=id))
    conn.commit()


def update_task_status(id, status):
    # updating status, executing
    cur.execute("UPDATE tasks SET status='{status}', updated_utc=now() at time zone 'utc' WHERE id={id}"
                .format(status=status, id=id))
    conn.commit()


def update_task_duration(id, duration):
    # updating status, executing
    cur.execute("UPDATE tasks SET duration='{d}' WHERE id={id}".format(d=round(duration, 3), id=id))
    conn.commit()


def get_nearest_file(fname):
    if os.path.exists(fname):
        return fname
    timestamp = os.path.splitext(ntpath.basename(fname))[0]
    directory = os.path.dirname(fname)

    for x in range(-2, 3):
        fnext = os.path.join(directory,str(Decimal(timestamp)+x) + ".ts")
        if os.path.exists(fnext):
            return fnext
    return ""


def process_one_bitrate(xml_task, source_directory, output_directory, media_object_id, bitrate, output_file_name, output_format):

    logging.info('Output directory: {directory_name}'.format(directory_name=output_directory))

    import xml.etree.ElementTree as ET
    root = ET.fromstring(xml_task)

    ffmpeg = config.get('Work','ffmpeg')

    # tmp_path = os.path.join(tempfile.gettempdir(), str(uuid.uuid4()))
    tmp_path = os.path.join(tempfile.gettempdir(), 'process_{id}'.format(id=media_object_id))

    if not os.path.isdir(tmp_path):
        os.makedirs(tmp_path)

    logging.info('Temp directory: {directory_name}'.format(directory_name=tmp_path))

    scenes = []

    file_path = os.path.join(source_directory, bitrate['bitrate'])

    #checking file existance
    for fn in root.iter('File'):
        full_name = os.path.join(file_path, fn.text.replace('\\', '/'))
        if not os.path.exists(full_name):
            logging.info("File not found: " + full_name + ", searching ...")
            fn = get_nearest_file(full_name)
            if fn:
                logging.info("File found: " + fn)
            else:
                msg = 'Source file does not exist: {file_name}'.format(file_name=full_name)
                raise CustomExceptiom(msg)

    i=0

    for n in root.iter('FfmpegScene'):

        i += 1
        scene_file_name = os.path.join(tmp_path, "scene%03d.ts" % i)

        #logging.info(scene_file_name)

        if os.path.exists(scene_file_name):
            os.remove(scene_file_name)

        type = n.find('Type').text
        file_name = n.find('FileName').text.replace('\\', '/')
        full_name = os.path.join(file_path, file_name)

        full_name = get_nearest_file(full_name)


        #logging.info(full_name)
        #logging.info(type)


        if not os.path.exists(full_name):
            msg = 'Source file does not exist: {file_name}'.format(file_name=full_name)
            raise CustomExceptiom(msg)

        if type=='B':

            # <FfmpegScene>
            #     <Type>B</Type>
            #     <FileName>201503101800.ts</FileName>
            #     <GopFrames>7425,7500</GopFrames>
            #     <GopFile>1</GopFile>
            #     <CutStart>1.36</CutStart>
            #     <Delay>0.062</Delay>
            # </FfmpegScene>

            gop_frames = n.find('GopFrames').text
            gop_file = n.find('GopFile').text
            cut_start = n.find('CutStart').text
            delay = n.find('Delay').text

            if Decimal(cut_start) + Decimal(delay) > 1.9:
                logging.info("Ignoring fragment, too small")
                continue

            #ffmpeg -i ../1200/201503101745.ts -c copy -f segment -reset_timestamps 1 -segment_frames 75,150 -map 0 test%0d.ts -y

            cmd = "nice -n 20 " + ffmpeg + " -i " + full_name + \
                  " -c copy -f segment -reset_timestamps 1 -segment_frames " + gop_frames + \
                  " -map 0 " + tmp_path + "/test%0d.ts -y"
            cmd += " && "

            cmd += "nice -n 20 " + ffmpeg + " -i " + tmp_path + "/test" + gop_file + ".ts " + \
                   " -ss " + str(Decimal(cut_start) + Decimal(delay)) + " " + bitrate['params'] + " " + \
                   scene_file_name + " -y"

            os_run_command(cmd)

        elif type=='F':

            # <FfmpegScene>
            #     <Type>F</Type>
            #     <FileName>201503101805.ts</FileName>
            # </FfmpegScene>

            copy(full_name, scene_file_name)

        elif type=='G':

            # <FfmpegScene>
            #     <Type>G</Type>
            #     <FileName>201503101825.ts</FileName>
            #     <GopFrames>1275,7500</GopFrames>
            #     <GopFile>1</GopFile>
            # </FfmpegScene>

            gop_frames = n.find('GopFrames').text
            gop_file = n.find('GopFile').text

            cmd = "nice -n 20 " + ffmpeg + " -i " + full_name + \
                  " -c copy -f segment -reset_timestamps 1 -segment_frames " + gop_frames + " -map 0 " + tmp_path + "/test%0d.ts -y"
            cmd += " && "

            cmd += "nice -n 20 " + ffmpeg + " -i " + tmp_path + "/test" + gop_file + ".ts " + " -c copy " + scene_file_name + " -y"

            os_run_command(cmd)

        elif type=='A':

            # <FfmpegScene>
            #     <Type>A</Type>
            #     <FileName>201503101820.ts</FileName>
            #     <GopFrames>4875,4950</GopFrames>
            #     <GopFile>1</GopFile>
            #     <CutEnd>1.72</CutEnd>
            #     <Delay>0.060</Delay>
            # </FfmpegScene>

            gop_frames = n.find('GopFrames').text
            gop_file = n.find('GopFile').text
            cut_end = n.find('CutEnd').text
            delay = n.find('Delay').text

            if Decimal(cut_end) < 0.1:
                logging.info("Ignoring fragment, too small")
                continue

            cmd = "nice -n 20 " + ffmpeg + " -i " + full_name + \
                  " -c copy -f segment -reset_timestamps 1 -segment_frames " + gop_frames + " -map 0 " + tmp_path + "/test%0d.ts -y"
            cmd += " && "

            cmd += "nice -n 20 " + ffmpeg + " -i " + tmp_path + "/test" + gop_file + ".ts " + \
                   "-ss " + delay + " -t " + cut_end + " " + bitrate['params'] + " " + scene_file_name + " -y"

            result = os_run_command(cmd)

        scenes.append(scene_file_name)

    # # this is a comment
    # file '/path/to/file1'
    # file '/path/to/file2'
    # file '/path/to/file3'

    # JOINING through Concat demuxer
    # list_file_name = os.path.join(tmp_path, 'list.lst')
    #
    # f = open(list_file_name,'w')
    #
    # for x in scenes:
    #     f.write("file '" + x + "'\n") # python will convert \n to os.linesep
    # f.close() # you can omit in most cases as the destructor will call if
    # cmd = "nice -n 20 " + ffmpeg + " -f concat -i " + list_file_name

    # encoding audio
    # ffmpeg -i ORT-13052015-0-1215_replace_1062327.ts -map 0:v -c:v copy -map 0:a:0 -c:a libvo_aacenc -b:a 160K -ac 2 -ar 44100

    # JOINING through Concat protocol
    cmd = "nice -n 20 " + ffmpeg + " -i \"concat:"
    cmd += '|'.join(x for x in scenes) + "\" "


    if output_file_name.endswith(".mp4"):
        tmp_out = os.path.join(tmp_path, "out.mp4")
        cmd += " -c copy -bsf:a aac_adtstoasc " + tmp_out + " -y"
    elif output_file_name.endswith(".ts"):
        tmp_out = os.path.join(tmp_path, "out.ts")
        cmd += " -c copy " + tmp_out + " -y"
        #cmd += " -map 0:v -c:v copy -map 0:a:0 -c:a libvo_aacenc -b:a 160K -ac 2 -ar 44100 " + tmp_out + " -y"

    os_run_command(cmd)

    if 'md5-' in output_file_name:
        f = open(tmp_out, 'rb')
        md5 = md5_for_file(f)
        output_file_name = output_file_name.replace("md5", md5);

    logging.info('Creating output file {file_name}'.format(file_name=output_file_name))
    copy(tmp_out, output_file_name)

    if not os.path.exists(output_file_name):
        msg = 'Output file does not exist: {file_name}'.format(file_name=output_file_name)
        logging.error(msg)
        update_task_status_error(id, msg)

    #deleting scene files
    for s in scenes:
        if os.path.exists(s):
            os.remove(s)

    for x in range(1,4):
        fname = tmp_path + "/test{0}.ts".format(x)
        if os.path.exists(fname):
            os.remove(fname)

    rmtree(tmp_path)

class CustomExceptiom(Exception):
    def __init__(self, m):
        self.message = m
    def __str__(self):
        return self.message


def get_connection_string(config):
    string = \
        "host=" + config.get('Database', 'host') + " " + \
        "port=" + config.get('Database', 'port') + " " + \
        "dbname=" + config.get('Database', 'db') + " " + \
        "user=" + config.get('Database', 'username') + " " + \
        "password=" + config.get('Database', 'password')
    return string


#----------------------------------------------------------------------
def create_timed_rotating_log(path):
    """"""
    logger = logging.getLogger("Rotating Log")
    logger.setLevel(logging.INFO)

    handler = TimedRotatingFileHandler(path,
                                       when="d",
                                       interval=1,
                                       backupCount=5)

    formatter = logging.Formatter('%(asctime)s - %(levelname)s %(message)s')
    handler.setFormatter(formatter)

    logger.addHandler(handler)

    return logger

def process_task(task_row):

    import time

    id = task_row[0]

    media_object_id = task_row[1]
    content = task_row[2]

    directory_in = task_row[3]
    directory_out_mp4 = task_row[4]
    directory_out_wmv = task_row[5]
    bitrates_mp4 = task_row[6]
    bitrates_wmv = task_row[7]
    source_code = task_row[8]

    if not os.path.isdir(directory_in):
        logging.error('Source {source} root directory does not exist: {dir}'.format(source=source_code, dir=directory_in))
        return

    if bitrates_mp4 and not os.path.isdir(directory_out_mp4):
        logging.error('MP4 output directory does not exist: {dir}'.format(source=source_code, dir=directory_out_mp4))
        return

    if bitrates_wmv and not os.path.isdir(directory_out_wmv):
        logging.error('WMV output directory does not exist: {dir}'.format(source=source_code, dir=directory_out_wmv))
        return

    logging.info('')
    logging.info('{:16s} {:10s}'.format('ID:', str(id)))
    logging.info('{:16s} {:10s}'.format('Media object ID:', str(media_object_id)))
    logging.info(content)
    logging.info('{:16s} {:10s}'.format('Directory in:', directory_in))

    x = json.loads(content)

    import base64
    xml_task = base64.b64decode(x['content'])

    import xml.etree.ElementTree as ET
    root = ET.fromstring(xml_task)

    duration = root.find('TotalDuration').text
    logging.info('{:16s} {:10s}'.format('Duration:', duration))

    air_time = root.find('AirDate').text
    logging.info('{:16s} {:10s}'.format('Air date:', air_time))



    start = time.time()

    try:
        update_task_status(id, 'executing')

        if bitrates_mp4:
            for bitrate in bitrates_mp4.split(','):

                logging.info('MP4 bitrates: {bitrates}'.format(bitrates=bitrates_mp4))
                logging.info('Processing MP4 bitrate: {bitrate}'.format(bitrate=bitrate))
                print 'Processing MP4 bitrate: {bitrate}'.format(bitrate=bitrate)

                b = bitrates_dict[bitrate]

                output_file_name = 'md5-{duration}-{bitrate_id}-{air_time}_replace_{media_object_id}.mp4'\
                    .format(duration=duration,air_time=air_time, media_object_id=media_object_id, bitrate_id=b['bitrate'])

                output_file_name = os.path.join(directory_out_mp4, output_file_name)

                process_one_bitrate(xml_task, directory_in, directory_out_mp4, media_object_id, b, output_file_name, 'mp4')

        if bitrates_wmv:
            for bitrate in bitrates_wmv.split(','):

                logging.info('WMV bitrates: {bitrates}'.format(bitrates=bitrates_wmv))
                logging.info('Processing WMV bitrate: {bitrate}'.format(bitrate=bitrate))
                print 'Processing WMV bitrate: {bitrate}'.format(bitrate=bitrate)

                b = bitrates_dict[bitrate]

                channel_dir = root.find('ChannelDirectory').text
                wmv_repl_name = root.find('WmvReplaceName').text

                logging.info("Channel firectory: {channel_dir}".format(channel_dir=channel_dir))
                logging.info("WMV replace name: {wmv_repl_name}".format(wmv_repl_name=wmv_repl_name))

                output_file_name = os.path.join(directory_out_wmv, wmv_repl_name) + ".ts"
                process_one_bitrate(xml_task, directory_in, directory_out_wmv, media_object_id, b, output_file_name, 'wmv')

        logging.info('Output file OK, saving task status...')
        update_task_status(id, 'completed')

        duration = time.time() - start
        update_task_duration(id, duration)
        logging.info('Task completed')
        logging.info('Time elapsed: {duration}'.format(duration=round(duration, 3)))
        print 'Task completed'

    except: # catch *all* exceptions
        e = sys.exc_info()
        msg = 'Error: %s %s % s' % (e[0], e[1], e[2])

        logging.error(msg)
        update_task_status_error(id, msg)

def build_dict(seq, key):
    return dict((d[key], dict(d, index=i)) for (i, d) in enumerate(seq))


def process_active_tasks():

    global conn
    # Connecting to PG server
    try:
        conn = psycopg2.connect(connection_string)
    except Exception as e:
        logging.error("Error connecting to db " + format(e))
        sys.exit()

    global cur
    cur = conn.cursor()
    # logging.info('Connected to Postgresql, ' + \
    #     'host:' + config.get('Database','host') + \
    #     ' db:' + config.get('Database','db'))

    cur.execute("select t.id, t.media_object_id, t.content, s.directory_in, s.directory_out_mp4, s.directory_out_wmv, s.bitrates_mp4, s.bitrates_wmv, t.source_code \
        from tasks t \
        left join sources s on t.source_code=s.code \
        where t.status='new' or t.status='error' and t.try_qty<5")

    rows = cur.fetchall()

    if len(rows) > 0:
        logging.info('Task(s) found: {task_count}'.format(task_count=len(rows)))

        global bitrates_dict
        bitrates_dict = build_dict(bitrates, key="bitrate")

        for row in rows:
            process_task(row)

    cur.close()
    conn.close()


# Starting....
current_path = os.path.dirname(os.path.realpath(__file__))
config_file = os.path.join(current_path, 'tasks.cfg')

try:
    config = ConfigParser.ConfigParser()
    config.read(config_file)
except:
    logging.error("Cannot load " + config_file)
    sys.exit()

log_path = config.get('Logging', 'path')
if not log_path:
    log_path = current_path

log_file = os.path.join(log_path, 'mtvl-publ-process-tasks.log')
logging = create_timed_rotating_log(log_file)

logging.info('Task started')

# encoding settings for ffmpeg
bitrates = []

# ffmpegParams = "-vsync 2 -map 0:v -filter:v \"yadif\" -filter:v \"scale={0}\" -c:v libx264 -threads 0 -fflags \"+genpts+igndts\" \
#     -x264opts bitrate={1}:keyint=75:vbv-maxrate={2}:vbv-bufsize={3}:scenecut=0 -map 0:a:0 -c:a libvo_aacenc -b:a 96K -ac 2 -ar 44100 \
#     -filter:a \"aresample=44100\" -vbsf h264_mp4toannexb -f mpegts -reset_timestamps 1 ";
# bitrates.append({'bitrate':'400', 'params':ffmpegParams.format("432:240", "400", "800", "200")})
# bitrates.append({'bitrate':'800', 'params':ffmpegParams.format("640:360", "900", "1800", "450")})
# bitrates.append({'bitrate':'1200', 'params':ffmpegParams.format("740:400", "1600", "3200", "800")})
# bitrates.append({'bitrate':'2500', 'params':ffmpegParams.format("960:540", "2600", "5200", "1300")})

# -fflags '+igndts+genpts' \
# -map 0:v -c:v libx264 -bsf:v h264_mp4toannexb -aspect 16:9 -filter:v "yadif,scale=480:270" \
# -b:v 400k -threads 8 -r 25 -g 50 -sc_threshold 0 -maxrate 800k -bufsize 800k \
# -map 0:a:1 -c:a libvo_aacenc -b:a 48k -ar 44100 -ac 1 \
# -hls_time 10 -hls_list_size 3 -hls_wrap 4 /var/www/ram/tmp/CH411/400_.m3u8 \
# -map 0:v -c:v libx264 -bsf:v h264_mp4toannexb -aspect 16:9 -filter:v "yadif,scale=640:360" \
# -b:v 900k -threads 8 -r 25 -g 50 -sc_threshold 0 -maxrate 1800k -bufsize 1800k \
# -map 0:a:1 -c:a libvo_aacenc -b:a 96k -ar 44100 -ac 2 \
# -hls_time 10 -hls_list_size 3 -hls_wrap 4 /var/www/ram/tmp/CH411/800_.m3u8 \
# -map 0:v -c:v libx264 -bsf:v h264_mp4toannexb -aspect 16:9 -filter:v "yadif,scale=960:540" \
# -b:v 1600k -threads 8 -r 25 -g 50 -sc_threshold 0 -maxrate 3200k -bufsize 3200k \
# -map 0:a:1 -c:a libvo_aacenc -b:a 128k -ar 44100 -ac 2 \
# -hls_time 10 -hls_list_size 3 -hls_wrap 4 /var/www/ram/tmp/CH411/1200_.m3u8 \
# -map 0:v -c:v libx264 -bsf:v h264_mp4toannexb -aspect 16:9 -filter:v "yadif,scale=960:540" \
# -b:v 2600k -threads 8 -r 25 -g 50 -sc_threshold 0 -maxrate 5200k -bufsize 5200k \
# -map 0:a:1 -c:a libvo_aacenc -b:a 160k -ar 44100 -ac 2 \
# -hls_time 10 -hls_list_size 3 -hls_wrap 4 /var/www/ram/tmp/CH411/2500_.m3u8 \

#ffmpegParams = "-fflags \"+genpts+igndts\" -map 0:v -c:v libx264 -bsf:v h264_mp4toannexb -aspect 16:9 -filter:v \"yadif,scale={0}\" -b:v {1}k -threads 8 -r 25 -g 50 -sc_threshold 0 -maxrate {2}k -bufsize {2}k -map 0:1 -c:a libvo_aacenc -b:a {4}k -ar 44100 -ac {3}"
ffmpegParams = "-fflags \"+genpts+igndts\" -map 0:v -c:v libx264 -bsf:v h264_mp4toannexb -aspect 16:9 -filter:v \"yadif,scale={0}\" -b:v {1}k -threads 8 -g 50 -sc_threshold 0 -maxrate {2}k -bufsize {2}k -map 0:1 -c:a libvo_aacenc -b:a {4}k -ar 48000 -ac {3}"

bitrates.append({'bitrate': '400', 'params':ffmpegParams.format("480:270", "400", "800", "1", "48")})
bitrates.append({'bitrate': '800', 'params':ffmpegParams.format("640:360", "900", "1800", "2", "96")})
bitrates.append({'bitrate': '1200', 'params':ffmpegParams.format("960:540", "1600", "3200", "2", "128")})
bitrates.append({'bitrate': '2500', 'params':ffmpegParams.format("960:540", "2600", "5200", "2", "160")})

logging.info('Config loaded')

connection_string = get_connection_string(config)

rt = RepeatedTimer(5, process_active_tasks)






