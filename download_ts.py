import os
import sys
import errno
import urllib2
import xmlrpclib
from datetime import datetime, timedelta
import ConfigParser
import logging
from logging.handlers import TimedRotatingFileHandler
import psycopg2
from threading import Timer


class RepeatedTimer(object):
    def __init__(self, interval, function, *args, **kwargs):
        self._timer = None
        self.interval = interval
        self.function = function
        self.args = args
        self.kwargs = kwargs
        self.is_running = False
        self.start()

    def _run(self):
        self.is_running = False
        self.start()
        self.function(*self.args, **self.kwargs)

    def start(self):
        if not self.is_running:
            self._timer = Timer(self.interval, self._run)
            self._timer.start()
            self.is_running = True

    def stop(self):
        self._timer.cancel()
        self.is_running = False


def unix_time(dt):
    epoch = datetime.utcfromtimestamp(0)
    delta = dt - epoch
    return int(delta.total_seconds())


def download_file(downloadfileurl, to_directory):
    if not os.path.isdir(to_directory):
        mkdir_p(to_directory)
    s = xmlrpclib.ServerProxy(xmlrpc_url)
    s.aria2.addUri([downloadfileurl], {'dir': to_directory})
    logging.info('Downloading task added: ' + downloadfileurl)


def get_all_downloads():
    s = xmlrpclib.ServerProxy(xmlrpc_url)
    r = s.aria2.tellActive() + s.aria2.tellWaiting(0, 10000)
    return r


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def get_connection_string(conf):
    string = \
        "host=" + conf.get('Database', 'host') + " " + \
        "port=" + conf.get('Database', 'port') + " " + \
        "dbname=" + conf.get('Database', 'db') + " " + \
        "user=" + conf.get('Database', 'username') + " " + \
        "password=" + conf.get('Database', 'password')
    return string


def create_timed_rotating_log(path):
    """"""
    logger = logging.getLogger("Rotating Log")
    logger.setLevel(logging.INFO)

    handler = TimedRotatingFileHandler(path,
                                       when="d",
                                       interval=1,
                                       backupCount=5)

    formatter = logging.Formatter('%(asctime)s - %(levelname)s %(message)s')
    handler.setFormatter(formatter)

    logger.addHandler(handler)

    return logger


def find_download(url, downloads):
    matches = next((x for x in downloads if x['files'][0]['uris'][0]['uri'] == url), None)
    return True if matches else False


def download_files():

    all_downloads = get_all_downloads()
    logging.info("Current download count: {count}".format(count=len(all_downloads)))

    for source in sources:

        check_interval = 5

        last_checked = datetime.utcnow() - source['last_checked']

        if last_checked.seconds > 600:
            check_interval = 30
            source['last_checked'] = datetime.utcnow()
            logging.info("Checking last 30 minutes, source {channel_alias}".format(channel_alias=source['channel_alias']))

        dtfrom = datetime.utcnow() - timedelta(minutes=check_interval)
        dtto = datetime.utcnow()

        for bitrate in source['bitrates_source'].split(','):

            url = 'http://{ip}/vod/{alias}/{bitrate}/{start}/{end}/playlist.m3u8' \
                .format(ip=shifter_ip,
                        start=unix_time(dtfrom),
                        end=unix_time(dtto),
                        bitrate=bitrate,
                        alias=source['channel_alias'])
            logging.info("Requesting playlist: {url}".format(url=url))

            try:
                response = urllib2.urlopen(url)
                html = response.read()

                for line in html.split('\n'):
                    if '.ts' in line:
                        relative_fname = line[:line.index('?')]
                        fname = os.path.join(source['directory_in'], bitrate, relative_fname)
                        fileurl = ('http://{ip}/{alias}/{bitrate}/' +
                           relative_fname).format(ip=shifter_ip,
                            bitrate=bitrate,
                            alias=source[
                                'channel_alias'])

                        if not find_download(fileurl, all_downloads) and not os.path.isfile(fname):
                            download_file(fileurl, os.path.dirname(fname))

            except urllib2.HTTPError, error:
                logging.error(error.url)
                logging.error("Error " + format(error))

            except Exception as error:
                logging.error("Error " + format(error))
                sys.exit()


# Starting....
current_path = os.path.dirname(os.path.realpath(__file__))
config_file = os.path.join(current_path, 'tasks.cfg')

try:
    config = ConfigParser.ConfigParser()
    config.read(config_file)
except:
    sys.exit()

log_path = config.get('Logging', 'path')
if not log_path:
    log_path = current_path

log_file = os.path.join(log_path, 'mtvl-publ-download-ts.log')
logging = create_timed_rotating_log(log_file)
logging.info('Task started')


logging.info('Config read')

connection_string = get_connection_string(config)

logging.info("Connecting to Postgresql DB")

# Connecting to PG server
try:
    conn = psycopg2.connect(connection_string)
except Exception as e:
    logging.error("Error connecting to db " + format(e))
    sys.exit()

cur = conn.cursor()
logging.info('Connected to Postgresql, ' +
             'host:' + config.get('Database', 'host') +
             ' db:' + config.get('Database', 'db'))

try:
    cur.execute("select code, directory_in, '400,800,1200,2500' as bitrates_source \
        from sources \
        where active")

    rows = cur.fetchall()

except Exception as e:
    logging.error("Error receiving data from db " + format(e))
    sys.exit()

logging.info('Sources found: {count}'.format(count=len(rows)))

sources = []

for row in rows:
    channel_alias = row[0]
    directory_in = row[1]
    bitrates_source = row[2]
    sources.append({'channel_alias': channel_alias, 'directory_in': directory_in, 'bitrates_source': bitrates_source, 'last_checked': datetime.utcnow()})
    logging.info('Source: {channel_alias}'.format(channel_alias=channel_alias))

cur.close()
conn.close()

xmlrpc_url = config.get('Download', 'xmlrpcurl')
shifter_ip = config.get('Download', 'shifter_ip')

rt = RepeatedTimer(10, download_files)

